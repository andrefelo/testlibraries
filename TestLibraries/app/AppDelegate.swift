//
//  AppDelegate.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//

import UIKit
import PatterCoordinator

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  
  var window: UIWindow?
  var mainCoordinator: MainCoordinator?
  
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    mainCoordinator = MainCoordinator()
    GlobalSetup.Api()
    guard #available(iOS 13, *) else {
      window = UIWindow()
      window?.rootViewController = mainCoordinator?.root
      window?.makeKeyAndVisible()
      mainCoordinator?.start(animated: false)
      return true
    }
    return true
  }
  
  
  func restartApp() {
    if #available(iOS 13, *) {
      guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let sceneDelegate = windowScene.delegate as? SceneDelegate
      else { return }
      sceneDelegate.restartApp()
    } else {
      mainCoordinator?.restart(animated: false, completion: {
        self.mainCoordinator = .init()
        self.window?.rootViewController = self.mainCoordinator?.root
        self.mainCoordinator?.start(animated: false)
      })
    }
  }
  
  // MARK: UISceneSession Lifecycle
  
  func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
  }
}

