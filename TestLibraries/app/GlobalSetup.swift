//
//  GlobalSetup.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//

import Foundation
import ToolsApiAFL

class GlobalSetup {
  
  class func Api () {
    ApiProvider.url = ConfigApp.Get().api.url
    ApiProvider.isDebug = ConfigApp.Get().api.isDebug
  }
}


struct ConfigApp: Codable {
  var api: Api
  
  struct Api: Codable {
    var url: String
    var isDebug: Bool
  }
  
  static func Get() -> Self {
    guard let url = Bundle.main.url(forResource: "Config", withExtension: "plist") else {
      fatalError("Could no find Config.plist in your bundle")
    }
    
    do {
      let data = try Data(contentsOf: url)
      let decoder = PropertyListDecoder()
      return try decoder.decode(ConfigApp.self, from: data)
    } catch  {
      fatalError("Some went wrong, review yor configuration")
    }
  }
}
