//
//  MainCoordinator.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//

import UIKit
import PatterCoordinator

class MainCoordinator: Coordinator {
  
  
  // ---------------------------------------------------------
  // MARK: Variables
  // ---------------------------------------------------------
  
  
  var uuid: String = UUID().uuidString
  var parent: Coordinator!
  var navigationController: UINavigationController = .init()
  var childCoordinators = [Coordinator]()
  
  
  // ---------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------
  
  
  init() {
    let vc = UIStoryboard(name: "LaunchScreen", bundle: nil).instantiateViewController(withIdentifier: "LaunchScreen")
    navigationController.viewControllers = [vc]
    navigationController.setNavigationBarHidden(true, animated: false)
  }
  
  
  // ---------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------
  
  
  func start(animated: Bool = false) {
    let coordinator = ProductCoordinator(withPresent: self, style: .fullScreen, animated: false)
    childCoordinators += [coordinator]
    coordinator.start(animated: animated)
  }
}
