//
//  SceneDelegate.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//

import UIKit
import PatterCoordinator

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
  
  var window: UIWindow?
  var mainCoordinator: Coordinator?
  
  
  func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
    // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
    // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
    // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
    guard
      let appDelegate = UIApplication.shared.delegate as? AppDelegate,
      let windowScene = (scene as? UIWindowScene)
    else { return }
    window = .init(windowScene: windowScene)
    window?.rootViewController = appDelegate.mainCoordinator?.root
    window?.makeKeyAndVisible()
    appDelegate.mainCoordinator?.start(animated: false)
  }
  
  
  func restartApp() {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
    else { return }
    appDelegate.mainCoordinator?.restart(animated: false, completion: {
      appDelegate.mainCoordinator = .init()
      self.window?.rootViewController = appDelegate.mainCoordinator?.root
      self.window?.makeKeyAndVisible()
      appDelegate.mainCoordinator?.start(animated: false)
    })
  }
}

