//
//  Coordinator.swift
//  TestLibraries
//
//  Created by Andres Lozano on 19/04/22.
//

//import UIKit
//
//
//public protocol Coordinator {
//  
//  
//  // ---------------------------------------------------------
//  // MARK: Variables
//  // ---------------------------------------------------------
//  
//  
//  var uuid:String { get set}
//  var parent:Coordinator! { get set }
//  var childCoordinators: [Coordinator] { get set }
//  var navigationController: UINavigationController { get set }
//  
//  
//  // ---------------------------------------------------------
//  // MARK: Helpers func
//  // ---------------------------------------------------------
//  
//  
//  /// funcion para inicializar el coordinador
//  /// - Parameter animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
//  func start(animated: Bool) -> Void
//}
//
//
//
//extension Coordinator {
//  
//  
//  // ---------------------------------------------------------
//  // MARK: Helpers func
//  // ---------------------------------------------------------
//  
//  
//  /// navigation controller del coordinator
//  public var root:UINavigationController {
//    return navigationController
//  }
//  
//  
//  /// Push ViewController
//  /// - Parameters:
//  ///   - viewController: The view controller to push onto the stack. This object cannot be a tab bar controller. If the view controller is already on the navigation stack, this method throws an exception.
//  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
//  public func push(_ viewController: UIViewController, animated: Bool = true) {
//    navigationController.pushViewController(viewController, animated: animated)
//  }
//  
//  
//  /// Present ViewController
//  /// - Parameters:
//  ///   - viewController: controlador que llega como parametro
//  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
//  ///   - completion
//  public func present(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
//    navigationController.present(viewController, animated: animated, completion: completion)
//  }
//  
//  
//  /// Close the ViewController doing a pop
//  /// - Parameter animated: define si se quiere mostrar la animación
//  public func pop(animated: Bool = true) {
//    navigationController.popViewController(animated: animated)
//  }
//  
//  
//  /// Close the ViewController doing a popToRoot
//  /// - Parameter animated: Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
//  public func popToRoot(animated: Bool = true) {
//    navigationController.popToRootViewController(animated: animated)
//  }
//  
//  
//  /// Close the ViewController doing a dismiss
//  /// - Parameters:
//  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
//  ///   - completion: se requiere hacer un proceso previo antes de finalizar la desvinculacion
//  public func dismiss(animated: Bool = true, completion: (() -> Void)? = nil) {
//    navigationController.dismiss(animated: animated, completion: completion)
//  }
//  
//  
//  /// Close the ViewController, this function checks what kind of presentation has the controller and then it make a dismiss or pop
//  /// - Parameters:
//  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
//  ///   - completion: se requiere hacer un proceso previo antes de finalizar la desvinculacion
//  public func close(isController: Bool = false, animated: Bool = true, completion: (() -> Void)? = nil) {
//    let ask = !isController ? root.isModal : root.viewControllers.last?.isModal == true
//    if ask {
//      dismiss(animated: animated) {
//        completion?()
//      }
//    } else {
//      pop(animated: animated)
//      completion?()
//    }
//  }
//  
//  
//  /// Remove its child coordinators
//  /// - Parameters:
//  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
//  ///   - coordinator: Coordinator
//  ///   - completion
//  mutating public func removeChild(animated: Bool = true, coordinator : Coordinator, completion:(() -> Void)? = nil) {
//    if let index = childCoordinators.firstIndex(where: {$0.uuid == coordinator.uuid}) {
//      coordinator.removeChids(animated: animated)
//      childCoordinators.remove(at: index )
//    }
//    completion?()
//  }
//  
//  
//  /// Remove its child coordinators
//  /// - Parameter animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
//  public func removeChids(animated: Bool = true, completion:(() -> Void)? = nil){
//    for coordinator in childCoordinators {
//      coordinator.close(animated: animated) {
//        coordinator.clearCoordinator()
//        coordinator.navigationController.viewControllers = []
//      }
//    }
//    completion?()
//  }
//  
//  
//  /// Close the current navigation controller and then removes it from its coordinator parent
//  /// - Parameters:
//  ///   - animated: Bool, Specify true to animate the transition or false if you do not want the transition to be animated. You might specify false if you are setting up the navigation controller at launch time.
//  ///   - completion
//  public func finish(animated: Bool = true, completion: (() -> Void)?) {
//    var aux = self
//    
//    if aux.parent != nil {
//        aux.parent?.removeChild(
//          animated: animated,
//          coordinator: self,
//          completion: {
//            self.close(animated: animated) {
//              self.clearCoordinator()
//             completion?()
//            }
//          }
//        )
//    } else {
//      aux.removeChids(animated: animated, completion: completion)
//      aux.childCoordinators.removeAll()
//    }
//  }
//  
//  
//  func clearCoordinator() {
//    if let item = self as? TabbarCoordinator {
//      item.tabController?.viewControllers = nil
//      item.tabController = nil
//      item.callBack = nil
//      item.firstCtrl = nil
//      item.root.viewControllers = []
//    } else if let item = self as? BaseCoordinator{
//      item.firstCtrl = nil
//      item.callBack = nil
//      item.root.viewControllers = []
//    }
//  }
//  
//  
//  /// Get the top coordinator
//  /// - Parameters:
//  ///   - appCoordinator: Main coordinator
//  ///   - pCoodinator:
//  public func topCoordinator(pCoodinator: Coordinator? = nil) -> Coordinator? {
//    
//    guard childCoordinators.last != nil else {
//      return self
//    }
//    
//    var auxCoordinator = pCoodinator ?? self.childCoordinators.last
//    
//    guard let tabCoordinator = auxCoordinator as? TabbarCoordinator
//    else {
//      return getDeepCoordinator(
//        from: &auxCoordinator
//      )
//    }
//    
//    let itemSelected        = tabCoordinator.tabController.selectedIndex
//    let coordinatorSelected = tabCoordinator.childCoordinators[itemSelected]
//    auxCoordinator          = coordinatorSelected.childCoordinators.last
//    
//    if let coord = auxCoordinator as? TabbarCoordinator {
//      return self.topCoordinator(pCoodinator: coord)
//    } else {
//      auxCoordinator = coordinatorSelected
//      return getDeepCoordinator(
//        from: &auxCoordinator
//      )
//    }
//  }
//  
//  /// Get the deepest coordinator from a given coordinator as parameter
//  /// - Parameters:
//  ///   - value: Coordinator
//  private func getDeepCoordinator(from value:inout Coordinator?) -> Coordinator?{
//    var rhs: Coordinator?
//    while rhs == nil {
//      if value?.childCoordinators.last == nil {
//        rhs = value
//      } else {
//        value = value?.childCoordinators.last
//      }
//    }
//    return rhs
//  }
//  
//  
//  func restart(animated: Bool, completion: (() -> Void)?) {
//    while true {
//      guard let coordinator = topCoordinator() else { return }
//      
//      if coordinator.parent == nil {
//        completion?()
//        return
//      } else {
//        coordinator.finish(animated: false, completion: nil)
//      }
//    }
//  }
//}
//
//
//
//
//open class BaseCoordinator: Coordinator {
//  
//  
//  // ---------------------------------------------------------
//  // MARK: Variables
//  // ---------------------------------------------------------
//  
//  
//  open var uuid: String
//  open var parent: Coordinator!
//  open var childCoordinators = [Coordinator]()
//  open var navigationController: UINavigationController = .init()
//  open var firstCtrl: UIViewController?
//  open var callBack: ((Any, String) -> ())?
//  
//  
//  // ---------------------------------------------------------
//  // MARK: Constructor
//  // ---------------------------------------------------------
//  
//  
//  public init(withTabs parent: Coordinator) {
//    self.parent = parent
//    uuid = NSStringFromClass(type(of: self))
//  }
//  
//  
//  public init(navigateWithParent parent: Coordinator) {
//    self.parent = parent
//    uuid = NSStringFromClass(type(of: self))
//    navigationController = parent.root
//  }
//  
//  
//  public init(withPresent parent: Coordinator, style: UIModalPresentationStyle = .fullScreen, animated: Bool = true) {
//    self.parent = parent
//    uuid = NSStringFromClass(type(of: self))
//    root.modalPresentationStyle = style
//    root.setNavigationBarHidden(true, animated: false)
//    self.parent.present(root, animated: animated)
//  }
//  
//  
//  // ---------------------------------------------------------
//  // MARK: Helpers func
//  // ---------------------------------------------------------
//  
//  
//  open func start(animated: Bool = true ) {
//    guard let ctrl = firstCtrl else {
//      fatalError("BaseTabBarCoordinator: firstCtrl is null")
//    }
//    push(ctrl, animated: animated)
//  }
//  
//  
//  open func initChildCoordinator(_ coordinator: Coordinator, animated: Bool = true){
//    childCoordinators += [coordinator]
//    coordinator.start(animated: animated)
//  }
//  
//  
//  deinit {
//#if DEBUG
//    print("Exit BaseTabBarCoordinator: \(uuid)")
//#endif
//  }
//}
//
//
//
//open class BaseTabBarCoordinator: BaseCoordinator {
//  
//  
//  // ---------------------------------------------------------
//  // MARK: Variables
//  // ---------------------------------------------------------
//  
//  
//  open var tabBarItem: UITabBarItem? {
//    didSet {
//      setupBarItem()
//    }
//  }
//  
//  open override var firstCtrl: UIViewController? {
//    didSet {
//      firstCtrl?.title = tabBarItem?.title
//    }
//  }
//  
//  
//  // ---------------------------------------------------------
//  // MARK: Constructor
//  // ---------------------------------------------------------
//  
//  
//  public override init(navigateWithParent parent: Coordinator) {
//    super.init(navigateWithParent: parent)
//  }
//  
//  
//  public override init(withTabs parent: Coordinator) {
//    super.init(withTabs: parent)
//  }
//  
//  
//  // ---------------------------------------------------------
//  // MARK: Helpers func
//  // ---------------------------------------------------------
//  
//  
//  private func setupBarItem() {
//    guard let item = tabBarItem else { return }
//    navigationController.tabBarItem = item
//  }
//  
//  
//  open override func start(animated: Bool = true) {
//    super.start()
//  }
//}
//
//
//
//open class TabbarCoordinator: BaseCoordinator {
//  
//  
//  public typealias T = UITabBarController
//  
//  
//  // ---------------------------------------------------------------------
//  // MARK: Variables
//  // ---------------------------------------------------------------------
//  
//  
//  open var tabController: T!
//  
//}
