//
//  ProductModel.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//

import Foundation

struct ProductModel: Codable {
  
  
  // ---------------------------------------------------------------------
  // MARK: Variables
  // ---------------------------------------------------------------------
  
  
  var description: String?
  var title: String?
  var image: String?
}



extension ProductModel: ProductListCellInfo {
  
  
  // ---------------------------------------------------------------------
  // MARK: InfoCounterListCell
  // ---------------------------------------------------------------------
  
  
  func getTitle() -> String {
    return title ?? " - "
  }
  
  
  func getDescription() -> String {
    return description ?? " - "
  }
  
  
  func getImage() -> URL? {
    guard let image = image else { return nil }
    return URL(string: image)
  }
}
