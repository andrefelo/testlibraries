//
//  CounterProviderProtocol.swift
//  TestLibraries
//
//  Created by Andres Lozano on 18/06/21.
//

import Foundation
import ToolsApiAFL

// ---------------------------------------------------------------------
// MARK: Protocols
// ---------------------------------------------------------------------


protocol ProductProviderProtocol {
  func all<T, E>(handleSuccess: @escaping HandleSuccess<T, E>)
}


typealias HandleSuccess<T:Codable, E: HError> =  (Result<T, E>) -> Void
//typealias HandleResponse<T:Codable, E: HError> =  (Result<T, E>)
