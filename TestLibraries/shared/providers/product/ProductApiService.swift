//
//  CounterApiService.swift
//  TestLibraries
//
//  Created by Andres Lozano on 15/06/21.
//

import Foundation
import ToolsApiAFL

class ProductApiService: ProductProviderProtocol {
  
  
  func all<T,E>(handleSuccess: @escaping HandleSuccess<T,E>) {
    ApiProvider.performRequest(
      route: CounterRouter.all,
      completion: handleSuccess
    )
  }
  
  
  enum CounterRouter: BaseRoute {
    
    // ---------------------------------------------------------
    // MARK: Cases
    // ---------------------------------------------------------
    
    
    case all
    
    
    // ---------------------------------------------------------
    // MARK: Helper funcs
    // ---------------------------------------------------------
    
    
    func getEndPoint() -> EndPoint? {
      
      
      switch self {
        case .all:
          return EndPoint(method: .get, path: "/list")
      }
    }
  }
}
