//
//  CounterProvider.swift
//  TestLibraries
//
//  Created by Andres Lozano on 15/06/21.
//

import Foundation
import ToolsApiAFL
import FundationExtensionsAFL


class ProductProvider {
  
  
  // ---------------------------------------------------------------------
  // MARK: Variables
  // ---------------------------------------------------------------------
  
  
  var dataSource: ProductProviderProtocol
  
  
  // ---------------------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------------------
  
  
  init(dataSource: ProductProviderProtocol = ProductApiService()) {
    self.dataSource = dataSource
  }
  
  
  // ---------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------
  
  
  func all(success: @escaping HandleSuccess<[ProductModel], ErrorModel>) {
    self.dataSource.all(handleSuccess: success)
  }
}
