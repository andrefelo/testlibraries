//
//  UIWindows+Helpers.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//

import UIKit



extension UIWindow {
  static var isLandscape: Bool {
    if #available(iOS 13.0, *) {
      return UIApplication.shared.windows
        .first?
        .windowScene?
        .interfaceOrientation
        .isLandscape ?? false
    } else {
      return UIApplication.shared.statusBarOrientation.isLandscape
    }
  }
}

