//
//  ProductListViewModel.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//

import Foundation
import ToolsAFL

class ProductListViewModel: NSObject {
  
  typealias ModelItem = ProductModel
  
  
  // ---------------------------------------------------------------------
  // MARK: Constants
  // ---------------------------------------------------------------------
  
  
  let coordinator: ProductCoordinator
  
  
  // ---------------------------------------------------------------------
  // MARK: Variables
  // ---------------------------------------------------------------------
  
  
  var items = Bindable<[ModelItem]>()
  var onError = Bindable<String>()
  var title: String {
    return "Product List"
  }
  
  
  
  // ---------------------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------------------
  

  init(coordinator: ProductCoordinator) {
    self.coordinator = coordinator
    super.init()
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  

  
  func fetchItems()  {
    let provider = ProductProvider()
    provider.all { [weak self ]response in
      switch response{
        case .success(let result):
          self?.items.value = result
          break
        case .failure(let error):
          self?.onError.value = error.localizedDescription
      }
    }
  }
  
}



extension ProductListViewModel {
  
  
  func numberOfRowsInSection(_ section: Int) -> Int {
    return items.value?.count ?? 0
  }
  
  
  func numberOfSections() -> Int {
    return 1
  }
  
  
  func getItemAt(_ indexPath: IndexPath) -> ModelItem? {
    return items.value?[indexPath.item]
  }
  
  
  func didSelectItemAt(_ indexPath: IndexPath) {
    guard let item = getItemAt(indexPath) else { return }
    coordinator.detailProduct(item: item)
  }
}
