//
//  ProductList1Cell.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//


import UIKit
import ViewControllersAFL
import SDWebImage

class ProductListCell: DefaultCollectionCell<ProductListCellInfo> {
  
  
  // ---------------------------------------------------------------------
  // MARK: IBOutlets variables
  // ---------------------------------------------------------------------
  
  
  
  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var descriptionLabel: UILabel!
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var containerView: UIView!
  
  
  
  // ---------------------------------------------------------------------
  // MARK: Life cycle
  // ---------------------------------------------------------------------
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    addShadow(radius: 10, shadowRadius: 8, offset: .zero)
    imageView.cornerRadius = 5
    containerView.cornerRadius = 10
  }
  
  
  override func prepareForReuse() {
    super.prepareForReuse()
    item = nil
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  override func setup() {
    guard let item = item else { return }
    descriptionLabel.text = item.getDescription()
    titleLabel.text = item.getTitle()
    setupImage(url: item.getImage())
  }
  
  
  func setupImage(url: URL?) {
    DispatchQueue.main.async { [weak self] in
      self?.imageView.sd_setImage(
        with: url,
        placeholderImage: .init(named: "placeholder")
      )
    }
  }
}



protocol ProductListCellInfo {
  func getTitle() -> String
  func getDescription() -> String
  func getImage() -> URL?
}
