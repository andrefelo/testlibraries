//
//  ProductListController.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//

import UIKit
import ViewControllersAFL
import UIKitExtensionsAFL

class ProductListController: DefaultCollectionController<ProductListViewModel> {
  
  
  // ---------------------------------------------------------------------
  // MARK: Life cycle
  // ---------------------------------------------------------------------
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = viewModel.title
  }
  
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    
    super.viewWillTransition(to: size, with: coordinator)
    
    coordinator.animate( alongsideTransition: { [weak self] _ in
      guard let self = self else { return }
      if UIWindow.isLandscape {
        self.cols = 2
        self.updateFlowLayout()
      } else {
        self.cols = 1
        self.updateFlowLayout()
      }
      self.collectionView.collectionViewLayout.invalidateLayout()
      
    }, completion: { _ in }
    )
  }
  
  
  override func loadView() {
    super.loadView()
    setupCollection()
    DispatchQueue.main.asyncAfter(deadline: .now() ) { [weak self] in
      self?.collectionView.beginRefreshing()
    }
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  func setupCollection() {
    
    setupRefreshCtrlCollection()
    let space: CGFloat = 10
    
    collectionInsets = .init(
      top: space * 3,
      left: space,
      bottom: space,
      right: space
    )
    
    minimumInterRow = space * 2
    
    if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
      layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
    }
    
    collectionView.registerFromNib(ProductListCell.self)
    collectionView.backgroundColor = .systemGray5
  }
  
  
  override func setupObservers() {
    
    viewModel.items.bind { [weak self] _ in
      DispatchQueue.main.async { [weak self] in
        self?.collectionView.endRefreshing()
        self?.collectionView.reloadData()
      }
    }
    
    viewModel.onError.bind { [weak self] msj in
      self?.showAlert(title: "Opss", message: msj)
    }
    
    onRefresh = { [weak self] in
      self?.viewModel.fetchItems()
    }
  }
  
  
  func showAlert(title: String?, message: String?) {
    
    let alert = UIAlertController(
      title: title,
      message: message,
      preferredStyle: .alert
    )
    
    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
    present(alert, animated: true)
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: UICollectionViewDataSource
  // ---------------------------------------------------------------------
  
  
  override func numberOfSections(in collectionView: UICollectionView) -> Int {
    return viewModel.numberOfSections()
  }
  
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModel.numberOfRowsInSection(section)
  }
  
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell: ProductListCell = collectionView.dequeueCell(indexPath: indexPath)
    cell.item = viewModel.getItemAt(indexPath)
    return cell
  }
  
  
  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    viewModel.didSelectItemAt(indexPath)
  }
}
