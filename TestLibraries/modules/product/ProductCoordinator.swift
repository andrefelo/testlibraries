//
//  ProductCoordinator.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//

import UIKit
import PatterCoordinator

class ProductCoordinator: BaseCoordinator {
  
  
  // ---------------------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------------------
  
  
  override init(withPresent parent: Coordinator, style: UIModalPresentationStyle = .fullScreen, animated: Bool = true) {
    super.init(withPresent: parent, style: style, animated: animated)
    root.setNavigationBarHidden(false, animated: false)
    root.navigationBar.prefersLargeTitles = true
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  override func start(animated: Bool = false) {
    let vm = ProductListViewModel(coordinator: self)
    firstCtrl = ProductListController(viewModel: vm)
    super.start(animated: animated)
  }
  
  
  func detailProduct(item: ProductModel)  {
    let vm = DetailProductViewModel(coordinator: self, item: item)
    let ctrl = DetailProductController(viewModel: vm)
    ctrl.modalPresentationStyle = .fullScreen
    push(ctrl)
  }
  
  
  func showProductList() {
    initChildCoordinator(ProductCoordinator2(withPresent: self), animated: true)
  }
  
  
  func goBack() {
    close(isController: false)
  }
  
  
  func restarApp() {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
    else { return }
    appDelegate.restartApp()
  }
}



class ProductCoordinator2: ProductCoordinator {
  
}


class ProductCoordinator3: ProductCoordinator {
  
}
