//
//  DetailProductController.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//

import UIKit
import ViewControllersAFL
import UIKitExtensionsAFL

class DetailProductController: ViewController<DetailProductViewModel> {
  
  // ---------------------------------------------------------------------
  // MARK: IBOutlets variables
  // ---------------------------------------------------------------------
  
  
  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var descriptionLabel: UILabel!
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var containerImageView: UIView!
  @IBOutlet var imageBackground: UIImageView!
  @IBOutlet var closeButton: UIButton!
  
  
  // ---------------------------------------------------------------------
  // MARK: Life cycle
  // ---------------------------------------------------------------------
  
  
  override func loadView() {
    super.loadView()
    closeButton.cornerRadius = closeButton.frame.width / 2
    imageView.cornerRadius = 12
    containerImageView.addShadow(radius: 12, shadowRadius: 10, offset: .init(width: 0, height: 0))
    setup()
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  func setup() {
    
    descriptionLabel.text = viewModel.getDescription()
    titleLabel.text = viewModel.getTitle()
    DispatchQueue.main.async {
      self.setupImage()
    }
  }
  
  
  func setupImage() {
    imageView.sd_setImage(
      with: viewModel.getIamge(),
      placeholderImage: UIImage(named: "placeholder")
    ) { [weak self ](image, _, _, _) in
      self?.imageBackground.image = self?.imageView.image
    }
  }
  
  
  @IBAction func closeActionButton(_ sender: UIButton) {
    viewModel.close()
  }
  
  
  @IBAction func close2ActionButton(_ sender: UIButton) {
    viewModel.close2()
  }
}
