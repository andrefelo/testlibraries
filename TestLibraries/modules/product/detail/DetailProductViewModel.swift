//
//  DetailProductViewModel.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//

import Foundation
import ToolsAFL

class DetailProductViewModel: NSObject {
  
  
  typealias ModelItem = ProductModel
  
  
  // ---------------------------------------------------------------------
  // MARK: Constants
  // ---------------------------------------------------------------------
  
  
  let coordinator: ProductCoordinator
  
  
  // ---------------------------------------------------------------------
  // MARK: Variables
  // ---------------------------------------------------------------------
  
  
  var item = Bindable<ModelItem>()
  
  
  // ---------------------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------------------
  
  
  init(coordinator: ProductCoordinator, item: ModelItem) {
    self.coordinator = coordinator
    super.init()
    self.item.value = item
    
  }
  
  
  func getTitle() -> String {
    return item.value?.getTitle() ?? " - "
  }
  
  func getDescription() -> String {
    return item.value?.getDescription() ?? " - "
  }
  
  func getIamge() -> URL? {
    return item.value?.getImage()
  }
  
  
  func close() {
    coordinator.close()
  }
  
  
  func close2() {
    coordinator.showProductList()
  }
  
}
