
import XCTest
@testable import TestLibraries

class ProductProviderTests: XCTestCase {
  
  
  
  func testGetItems() throws {
    let expt = XCTestExpectation()
    let sut = ProductProvider()
    sut.all { result in
      switch result {
        case .success(let items):
          XCTAssert(!items.isEmpty)
          XCTAssertTrue(true)
          expt.fulfill()
          break
        case .failure(let error):
          XCTFail("test6ListCounter: Expected to be a success but got a failure with: \(error.localizedDescription)")
          expt.fulfill()
          break
      }
    }
    
  }
  
}
