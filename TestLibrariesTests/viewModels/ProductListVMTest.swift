//
//  ProductListVMTest.swift
//  TestLibrariesTests
//
//  Created by Andres Lozano on 3/01/22.
//

import XCTest
@testable import TestLibraries

class ProductListVMTest: XCTestCase {

  var sut: ProductListViewModel!
  
  
  override func setUp() {
    sut = ProductListViewModel.init(coordinator: .init(withPresent: MainCoordinator()))
  }
  
  override func tearDown() {
    sut = nil
  }
  
  
  func test_fetchItems() throws {
    let expt = XCTestExpectation()
    sut.items.bind { result in
      XCTAssertNotNil(result)
      expt.fulfill()
    }
    sut.fetchItems()
    wait(for: [expt], timeout: 10)
  }

}
